from buildstream import Source, SourceError, SourceFetcher, Consistency, utils
import os
import urllib.error
import urllib.request
import json
import hashlib
import tarfile


class FirefoxLocaleFetcher(SourceFetcher):

    def __init__(self, parent, locale, revision, sha256):
        self.locale = locale
        self.revision = revision
        self.sha256 = sha256
        self.parent = parent
        super(FirefoxLocaleFetcher, self).__init__()

    def fetch(self, alias_override=None, **kwargs):
        self.parent._fetch_cache(self.locale, self.revision, sha256=self.sha256)

class FirefoxLocalesSource(Source):

    BST_REQUIRED_VERSION_MAJOR = 1
    BST_REQUIRED_VERSION_MINOR = 3
    BST_REQUIRES_PREVIOUS_SOURCES_TRACK = True

    def _cache_file_name(self, locale, revision):
        return os.path.join(self.get_mirror_directory(), f'{locale}-{revision}.tar.bz2')

    def _verify_cache(self, locale, revision, sha256):
        h = hashlib.sha256()
        try:
            with open(self._cache_file_name(locale, revision), 'rb') as f:
                while True:
                    data = f.read(4096)
                    if len(data) <= 0:
                        break
                    h.update(data)
            return h.hexdigest() == sha256
        except FileNotFoundError:
            return False

    def _fetch_cache(self, locale, revision, *, sha256=None):
        h = hashlib.sha256()
        uri = self.uri_template.format(locale=locale, revision=revision)
        with utils.save_file_atomic(self._cache_file_name(locale, revision), mode='wb') as f:
            try:
                with urllib.request.urlopen(uri) as resp:
                    while True:
                        data = resp.read(4096)
                        if len(data) <= 0:
                            break
                        h.update(data)
                        f.write(data)

            except (urllib.error.URLError, urllib.error.HTTPError) as e:
                raise SourceError("{} failed: {}".format(uri, e.reason)) from e
            if sha256:
                if h.hexdigest() != sha256:
                    unexpected = h.hexdigest()
                    raise SourceError(f"{uri} returned data with unexpected hash '{unexpected}'. Expected '{sha256}'.")
        return h.hexdigest()

    def configure(self, node):

        self.ref = self.node_get_member(node, list, 'ref', None)
        self.uri_template = self.node_get_member(node, str, 'uri-template', 'https://hg.mozilla.org/l10n-central/{locale}/archive/{revision}.tar.bz2')
        self.l10n_changesets_path = self.node_get_member(node, str, 'l10n-changesets', 'browser/locales/l10n-changesets.json')
        self.l10n_changesets_path = os.path.relpath(os.path.join('/', self.l10n_changesets_path), '/')

        self.node_validate(node, Source.COMMON_CONFIG_KEYS + ['ref', 'uri-template', 'l10n-changesets'])

        if self.ref:
            for r in self.ref:
                self.node_validate(r, ['locale', 'revision', 'sha256'])

    def get_unique_key(self):
        return {
            'ref': {r['locale']: (r['revision'], r['sha256']) for r in self.ref},
            'uri-template': self.uri_template
        }

    def preflight(self):
        pass

    def load_ref(self, node):
        self.ref = self.node_get_member(node, list, 'ref', None)

    def get_ref(self):
        return self.ref

    def set_ref(self, ref, node):
        node['ref'] = self.ref = ref

    def track(self, previous_sources_dir):
        with open(os.path.join(previous_sources_dir, self.l10n_changesets_path), 'r') as f:
            changesets = json.load(f)
        ref = []
        for locale, data in changesets.items():
            if 'linux' not in data['platforms']:
                assert 'linux64' not in data['platforms']
                continue
            assert 'linux64' in data['platforms']
            revision = data['revision']
            self.info(f"Tracking {locale} {revision}")
            sha256 = self._fetch_cache(locale, revision, sha256=None)
            ref.append({'locale': locale,
                        'revision': revision,
                        'sha256': sha256})

        return ref

    def stage(self, directory):
        for r in self.ref:
            locale = r['locale']
            revision = r['revision']
            sha256 = r['sha256']
            if not self._verify_cache(locale, revision, sha256):
                raise SourceError(f"Locale {locale} has unexpected hash '{unexpected}'. Expected '{sha256}'.")
            with tarfile.open(self._cache_file_name(locale, revision), mode='r:bz2') as tar:
                tar.extractall(path=os.path.join(os.path.join(directory, f'locales/{locale}')))


    def get_consistency(self):
        if self.ref is None:
            return Consistency.INCONSISTENT

        for r in self.ref:
            if not self._verify_cache(r['locale'], r['revision'], r['sha256']):
                return Consistency.RESOLVED
        return Consistency.CACHED

    def get_source_fetchers(self):
        for r in self.ref:
            yield FirefoxLocaleFetcher(self, r['locale'], r['revision'], r['sha256'])


def setup():
    return FirefoxLocalesSource
