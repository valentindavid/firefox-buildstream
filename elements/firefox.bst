kind: manual

depends:
- filename: components/ca-certificates.bst
  junction: freedesktop-sdk.bst

- filename: components/tar.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/nasm.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: python2.bst
  type: build
- filename: components/which.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/zip.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/unzip.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/llvm.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/pkg-config.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/perl.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: extensions/rust/rust.bst
  junction: freedesktop-sdk.bst
  type: build

- filename: components/nss.bst
  junction: freedesktop-sdk.bst
- filename: components/libpulse.bst
  junction: freedesktop-sdk.bst
- filename: components/gtk3.bst
  junction: freedesktop-sdk.bst
- filename: components/sqlite.bst
  junction: freedesktop-sdk.bst
- filename: components/libwebp.bst
  junction: freedesktop-sdk.bst
- filename: components/xorg-lib-x11.bst
  junction: freedesktop-sdk.bst
- filename: components/xorg-lib-xext.bst
  junction: freedesktop-sdk.bst
- filename: components/xorg-lib-xt.bst
  junction: freedesktop-sdk.bst
- filename: components/xorg-lib-xcursor.bst
  junction: freedesktop-sdk.bst
#- filename: components/libvpx.bst
#  junction: freedesktop-sdk.bst

- filename: autoconf-2.13.bst
  type: build
- filename: nodejs.bst
  type: build
- filename: yasm.bst
  type: build
- filename: cbindgen.bst
  type: build

- filename: icu.bst
- filename: libevent.bst
- filename: dbus-glib.bst
- filename: gtk2.bst
- filename: startup-notification.bst

- filename: components/gdb.bst
  junction: freedesktop-sdk.bst
  type: build

- filename: components/dejavu-fonts.bst
  junction: freedesktop-sdk.bst
  type: runtime
- filename: components/emoji-one-font.bst
  junction: freedesktop-sdk.bst
  type: runtime
- filename: components/gnu-free-fonts.bst
  junction: freedesktop-sdk.bst
  type: runtime
- filename: components/liberation-fonts.bst
  junction: freedesktop-sdk.bst
  type: runtime

- filename: app-lib.bst
  type: runtime

environment-nocache:
- MAXJOBS

environment:
  MAXJOBS: '%{max-jobs}'
  PKG_CONFIG_PATH: "%{libdir}/pkgconfig:"
  CC: clang
  CXX: clang++
  AR: llvm-ar
  NM: llvm-nm
  RANLIB: llvm-ranlib
  CPPFLAGS: "-I/app/include"
  LDFLAGS: "-L/app/lib"
  PATH: '/usr/lib/sdk/rust/bin:/app/bin:/usr/bin'

config:
  configure-commands:
  - |
    cat >mozconfig <<EOF
    ac_add_options --prefix=/app
    ac_add_options --host=x86_64-unknown-linux-gnu
    ac_add_options --target=x86_64-unknown-linux-gnu
    ac_add_options --enable-application=browser
    ac_add_options --enable-release
    ac_add_options --with-system-zlib
    ac_add_options --disable-gconf
    ac_add_options --enable-readline
    ac_add_options --enable-startup-notification
    ac_add_options --enable-system-ffi
    ac_add_options --with-system-libevent
    ac_add_options --with-system-nspr
    ac_add_options --with-system-nss
    ac_add_options --enable-system-sqlite
    #ac_add_options --with-system-libvpx
    ac_add_options --with-system-webp
    ac_add_options --with-system-icu
    ac_add_options --disable-updater
    ac_add_options --disable-crashreporter
    ac_add_options --with-unsigned-addon-scopes=app,system
    ac_add_options --disable-alsa
    ac_add_options --enable-pulseaudio
    # TODO: enable this. Requires api keys
    ac_add_options --disable-necko-wifi

    ac_add_options --enable-system-pixman
    ac_add_options --with-system-bz2
    ac_add_options --with-system-jpeg
    ac_add_options --with-system-png

    ac_add_options --enable-optimize
    #Does not work with 1.33.0
    #ac_add_options --enable-rust-simd
    ac_add_options --enable-debug-symbols

    ac_add_options --disable-tests

    ac_add_options --disable-strip
    ac_add_options --disable-install-strip

    ac_add_options --with-l10n-base="%{build-root}/locales"

    # TODO: request permission to release an officially branded build
    if [ "%{branding}" = "official" ]; then
      ac_add_options --enable-official-branding
    else
      ac_add_options --with-branding=browser/branding/%{branding}
    fi

    mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/build-dir
    EOF

  - |
    ./mach configure

  - |
    sed -e 's/checkImpl/checkFFImpl/g' -i js/src/vm/JSContext*.h

  build-commands:
  - |
    ./mach build -j${MAXJOBS} --verbose

  - |
    for locale in locales/*; do
       make -C build-dir/browser/locales langpack \
         AB_CD="$(basename "${locale}")" \
         PKG_LANGPACK_BASENAME='$(MOZ_LANGPACK_EID)' \
         PKG_LANGPACK_PATH=xpi/
    done

  install-commands:
  - |
    DESTDIR="%{install-root}" ./mach install

  - |
    install -m 0644 -D -t "%{install-root}%{libdir}/firefox/browser/extensions/" build-dir/dist/xpi/langpack-*@*.mozilla.org.xpi

  - |
    install -m 0644 -D -t "%{install-root}%{libdir}/firefox/defaults/pref" firefox-configuration/firefox.js

sources:
- kind: tar
  url: https://archive.mozilla.org/pub/firefox/releases/69.0/source/firefox-69.0.source.tar.xz
  ref: 413c3febdfeb69eade818824eecbdb11eaeda71de229573810afd641ba741ec5
  base-dir: firefox-69.0
- kind: patch
  path: patches/firefox-enable-system-extensions.patch
- kind: patch
  path: patches/firefox-rustc-1-33-0.patch
- kind: patch
  path: patches/nodejs-nonblock.patch
- kind: local
  path: files/firefox-configuration/firefox.js
  directory: "firefox-configuration"

- kind: firefox_locales
  uri-template: https://hg.mozilla.org/l10n-central/{locale}/archive/{revision}.tar.bz2
  l10n-changesets: browser/locales/l10n-changesets.json
  ref:
  - locale: ach
    revision: 283e51ffe6b9
    sha256: e372ce320533a97740b0cdeacab66ce83574bda317871a6194826682fbc76270
  - locale: af
    revision: 432b5ca2c2ed
    sha256: 787c6f90fb8df9a01c512c6b374069b9e4f16a27e53c660ce512c83cfd81d147
  - locale: an
    revision: df538233ebbb
    sha256: 4767cf00094c5450667fe7705f19e0d088e493dcff257ee170538e24efb33680
  - locale: ar
    revision: 8de7cc60fc6a
    sha256: d8223dbe0c26a911b0b70d1095c1a0fd948f23418fb2d3d3f40acd901a616913
  - locale: ast
    revision: 2b0ce39c67b2
    sha256: 2e6dfaa1aad5ed0acf1b81b05c6dfe437bccd41a549e924c323b58f339c3c754
  - locale: az
    revision: 037002d300fa
    sha256: f8310a6a9c7895b71b54b8176045e2c7592470bbabbc2fa0e50c256092230576
  - locale: be
    revision: fbe2abe72208
    sha256: 94a1de0bf4003c67b82ad98956a6530c128f2fdea9ec93bf60c6607264bc159e
  - locale: bg
    revision: 98c42c1e9342
    sha256: f1752a46b1a84f5c72007250f1e12c051f0adf2a0db030f586d60b0c6373b04e
  - locale: bn
    revision: e27e45641eb7
    sha256: 0ff844e32a4c5a218a258379bbd43a31624e0fc1829813723c61656641df28ee
  - locale: br
    revision: 64c36445645f
    sha256: 4dc25e08d06157993013e0376de4a6caefa38bf55af5ffa183a3747fb7b20123
  - locale: bs
    revision: 49a6d69dec21
    sha256: cc1196b3d9b1ade600e31fb448114b328e71d348d7a0a70aca12cc70bc53c8fc
  - locale: ca
    revision: fed9901c8f97
    sha256: aa9ea817751136651033a3cc89e5e77d8b826e9cbf45c1b5ba6f8fa29413d543
  - locale: cak
    revision: 7d210e01354d
    sha256: 0e3d642fb51f8536b49dd05aa1ab358d1312fd62dfa34ee16f2dc1181244d44d
  - locale: cs
    revision: 4a862891859e
    sha256: 0741ec9c428454dfa3f9dbea3f59660787a62aed95db7040212372bb41a4d885
  - locale: cy
    revision: 1b9e944f37f5
    sha256: 4ac9047ef3a86e39ade60cfad0249d39cbf342119aa3c7fd18456a4fb9c959f2
  - locale: da
    revision: f0b4e7bf76f1
    sha256: 84aefadbb79c24f339a5516ce294495c4b001d2e328e9fa1de8a121f56a6898d
  - locale: de
    revision: 072dc80f435a
    sha256: a42d3e65b8fe491b57818a2befec085432315a4097bc436824c64c6167a13d27
  - locale: dsb
    revision: ee1d47f04f07
    sha256: 7bb9a3eb84e359dc6e06e76afc5590c4b8901b6357b23ca9e3ae5f79874e4a2c
  - locale: el
    revision: cc321f230c6d
    sha256: 8453f23d4e38a43082b8c7d21a3fa7c5c4bb8536b59e666957990d1ca3edfb0a
  - locale: en-CA
    revision: dad7b62f9dbc
    sha256: 86d621f37664eb5384ac0044fbae0e063c5d97b4334a4fdaae3bc861c166e11a
  - locale: en-GB
    revision: 8d7c3c919497
    sha256: 03af2ef299326b03d54a548fa1769beb0456d909790211245daabb9876834bfd
  - locale: eo
    revision: c3df54b21746
    sha256: d9233b2ff25f7fc55cd4c9dc4fc2ce6bc9434cdce520dfcc6b11395bbda1aa67
  - locale: es-AR
    revision: 39cf4ec0b778
    sha256: 572704ca0392316cd85f80e804ab001efe72b52b60c75754cf73bdfebf2f0394
  - locale: es-CL
    revision: 09ee2d8081e2
    sha256: ad491ae4c205ad559d08558348e36324362107eb23246ebd2c62d48832435af0
  - locale: es-ES
    revision: dd46a1c85278
    sha256: 041d719f62f447a04600f660424b0eabb39a26727f8dbeb6729b61a76a00a95f
  - locale: es-MX
    revision: 0b424422a300
    sha256: 027d91424aa1af989600f51ce1e328082b02d37e1b4aacfeb3c084d459791b86
  - locale: et
    revision: fd1fa5a944ab
    sha256: 51d17fc65651603588cec31a5c78fb1720e5b2a8b0f8f1b3d909d3b47211a25a
  - locale: eu
    revision: 9c39203cd09a
    sha256: 1658e293f28a12e77f553964d8151109aaac0113dccfa1ca8719bd9cc830b7bd
  - locale: fa
    revision: 8b8fef934787
    sha256: 7b69dcf7ae948e5da3af7df8a164ce3f018f2480d225bdaf7ff788b5219ed74d
  - locale: ff
    revision: 60498ef096e8
    sha256: 113ab7390414c15d384626f599957ab7ab464b3bb171d597d9db9638fe6f1118
  - locale: fi
    revision: d9e3a36d5803
    sha256: 55698c6a14f0ab2bafa179318cabb3e6033e1802ebfc2f41968f3b0df945e742
  - locale: fr
    revision: 6878c4e51516
    sha256: 5b42ee03f4016d9dc56e48efbf8e44bd2fd4ba16c2489e86b13730a9ae089521
  - locale: fy-NL
    revision: e76aac6f79c8
    sha256: c2aa52eaa7bae11a6845f5ae0d7db0efb2ac885d3534ecf0031ce4cfb1cb8a39
  - locale: ga-IE
    revision: 2a08a48e6adf
    sha256: 97a812c26ac0c4ceddc34b49bbdb69ae92def36b3af9e63750ed329918c801d4
  - locale: gd
    revision: 6de51f7d03e4
    sha256: 50f79150bdd1854724f6ad026747fd08d7ee926da562d11832b4364010380840
  - locale: gl
    revision: 51abb035d2b0
    sha256: 80a6ea8c1de96d09c9ed36e6319c9ab6b091ca2e065b60045bbe8069e5d5b999
  - locale: gn
    revision: 79352757dec1
    sha256: 662d9abb15aa57467fbe40f4574837558f575a0d3effefb9afbed727409d2e75
  - locale: gu-IN
    revision: f084a3d45eee
    sha256: 0ea1c419f0ab2e24ef1270653677a489c1a1d7c17ff927554677440e28f1120f
  - locale: he
    revision: 906ed6dde59c
    sha256: c7325ec3d24da7716b9f348bb0d70a7575e7d5d718f9ff28fafac8cfefc107a5
  - locale: hi-IN
    revision: a1e47d932903
    sha256: 4d2f3ccc7632317ea316af8b1bb7e45aae3161d79c2abeb6bc2c667b8cdda408
  - locale: hr
    revision: 5fe15a6d053c
    sha256: 58a3677f174c5137b201871c1534e27e6df9a0fe270a09800d0888165ca5092c
  - locale: hsb
    revision: 8e5b3f80dff0
    sha256: 122fc17cbfc5f34476ee93dcd04b40728ea25b97b83570cc9db9c6c4738f6443
  - locale: hu
    revision: dd44c146951d
    sha256: 6d16bc3b65e4711fb99206fa093a8d0e5e7acd1a181b2046a4c9d7cc3500152e
  - locale: hy-AM
    revision: b3ffe097092e
    sha256: 5cb844c7e889662d42d5f543f1df7d4ca51f1aaa269fa82ae0564a811b58c77e
  - locale: ia
    revision: 62020a85aa0e
    sha256: 21f75547bd4b38caf5c3472a797815c1a3915484953706ac96692145b42b4d9a
  - locale: id
    revision: 68003cc25555
    sha256: 25a532f62dcbd291284cef1c57762bb49d57930198c8d33518aa53cbdafc5509
  - locale: is
    revision: a87a390edd3e
    sha256: eba56f9793e7573e8140b81ada84b55fc39a2c4deabe5cbfca8e3cc2f36dbfe3
  - locale: it
    revision: 332f86817761
    sha256: a78355e779843e452ae6360dcf525851ace77ba8a319fc4bf7ab0144459747f0
  - locale: ja
    revision: c8856ae0cd93
    sha256: a616d6631d169e45510601b2eead4ac6b7e053899c3412eafc96dbcd1c2f9e11
  - locale: ka
    revision: e006a9f8c8da
    sha256: b67ba7ffd79b2627d8bc1218ea41953d908a59893c6bb04e851a1ac30a59e321
  - locale: kab
    revision: 96414bc72789
    sha256: 84155a081994042d9bc5c2a283d47230fc6f368fa39ec6934a58a64a6e43790f
  - locale: kk
    revision: 46f6e213653c
    sha256: c2834bdc30645595ce42681188970cd9b7ae0f990a3c58469e94c55aebb67f39
  - locale: km
    revision: c80766ed8f7a
    sha256: a80e825fc6f9da648032f3d40c154a8e733064d3dc0d93d30674834c1762cdd9
  - locale: kn
    revision: 1ad904e8dd43
    sha256: 002a9d7ea0143543c9b505b386afa83cfc614a39ef66f6acf7424d159e3e1251
  - locale: ko
    revision: 937d0c129e96
    sha256: 92077fadef8b386a5a59435b46e50874f465c4fc431db0fdca38e1bf89f265e9
  - locale: lij
    revision: f19a1fe663a5
    sha256: b317e1f52e3eed39e46a62961c555ee872c83fca39d5401c74ec976c12183df3
  - locale: lt
    revision: a3619c6b5b63
    sha256: 3cd765e4838055c8e5aadb36a5ddfab0c1e4f565e5e5254d793d5f7aa2878007
  - locale: lv
    revision: 2b77b6b05a34
    sha256: 8676bf94eca2e9dbdc600813ee474aadc15661db5c0f8c18ac22f441d0262c3a
  - locale: mk
    revision: 6e863a8d415a
    sha256: 53893be5a2dad2b87aca58108a7fce2041b4efe857e49d51c4334a18497a7d9a
  - locale: mr
    revision: 91cc067b4d91
    sha256: 04ba760e6b27d38c0d705f91092ae8a57989f807bb65179e52fee0e77fab409e
  - locale: ms
    revision: dca5d11f4923
    sha256: 96c4deacf6f50de65c53a6956d6195a225edb91813dc59d869e4f703b024be70
  - locale: my
    revision: a8fce50522fe
    sha256: ff0cb6c3144a541624540db87e56862f67e75312d6c632d18e4a62cfd0179147
  - locale: nb-NO
    revision: 61d5fbf22298
    sha256: 96f45d547c322195be78da667b87a8d077f678b662df794f0be83e1b0f48bd93
  - locale: ne-NP
    revision: ed5e3f5ff1e3
    sha256: 1bbdd64097f8fc8b5a05744007b9e2f9c4c144f3d15cd611c6cdaa3e5f76f6ae
  - locale: nl
    revision: 64abfab2deb6
    sha256: 92804b255e2c80b0de93fbc82a4cbd9dfedf3e63489c5fdd385c82378f08c166
  - locale: nn-NO
    revision: 06c0f31b9e20
    sha256: 1c07f62e2d20302c17aaf2a9a530a965c65377b6aa33cc22db697105707f1994
  - locale: oc
    revision: 128ab0a38965
    sha256: 162ee0a9b47ca873e56efcf21859cdd84b21067dd32fd4e33e813f19c582fa16
  - locale: pa-IN
    revision: ba90b66cd4e8
    sha256: 53cb507065721341bb6640db9511717579276004fd3eadb3f66602591ac7b346
  - locale: pl
    revision: 99679096f2e8
    sha256: 3447118b75757c6520de1436e7612efbee614ac43bf96cd00923a59bb3cebb1e
  - locale: pt-BR
    revision: 3697e7f211a4
    sha256: 2d651cc1068648d15edc9bcb0c369dbb8c3864f64b837faba55d312012ef1059
  - locale: pt-PT
    revision: c39af4c94da4
    sha256: f663f2c870417f6db13c5f8cac355876b4d24f42b815368e6ea48bbe55a224d7
  - locale: rm
    revision: e816d90fd94a
    sha256: 9191e6d7d14da332dfac0263825db2e2a01762b725251e31e87526165ff57c8c
  - locale: ro
    revision: 835c52c9c6c7
    sha256: 8ef880e5f7330b3b27a90dafa966d6360a1e8ff9ea8bd1b4c56eb43f9f017faf
  - locale: ru
    revision: 85a82b978604
    sha256: 34f64c27cb917255f5b44f71788149b6c7aa7197e2002a2be6c609090e7cb1de
  - locale: si
    revision: 97c1e72725b6
    sha256: 563ca51446343808706b50744e253fb9b478abe8d9e6e89f21001a6355a3d6ef
  - locale: sk
    revision: 0518c1b061bb
    sha256: b7eca5a0817d99e93e5ac097654020e6e4b79a2ab1767868103c8c0bf6e26d6a
  - locale: sl
    revision: 29f8c6a7d081
    sha256: 0c77fd28c601f1c28125c06004bb80d8041e7c7b3c420aa6b8be7f983f3f694a
  - locale: son
    revision: 0a65cc8e1faf
    sha256: 7ffdf84f8856451b2869922b3e6d98bf03949584c82569f5994cc0cc6cc12b1e
  - locale: sq
    revision: eb54b9ac7232
    sha256: 3c2255629f9c42dd6616467a5329ae5f5105aade844962a9b381f688dfc1b0d9
  - locale: sr
    revision: be009c805e18
    sha256: b6aeadd2f055e58bc217c4d950613c58d87de08103ca0841349c3afbf28ca051
  - locale: sv-SE
    revision: e0d33548f40d
    sha256: 36a8a44a0f6bcdb7f3045fd3e18bed6aeac32caf014550fa2e4ee267bac8567b
  - locale: ta
    revision: a4a70b06dc29
    sha256: 85dca40b3769eaf86c1dcf25f31aa5c0ee563f847f111f0438aa2e63370d9446
  - locale: te
    revision: d62da0dfae08
    sha256: 6be0765e861ca8cf5ba86ce755324c60c379455541b1b353eb3c1e1a5379e158
  - locale: th
    revision: 176155e69eac
    sha256: 0779eb9fb4248949c26fc816bdb37c99c7a6a791cbdad575b24d8a46932d7c3c
  - locale: tr
    revision: 2c5ddacf96f6
    sha256: 0ae75df2459c6afb297a48ba0f3c6b98ee0b52fc56a2f791b75bc762dde3ea25
  - locale: uk
    revision: 502d6e23d3a3
    sha256: f9c45f327bd3943abedcb537f1acb75b844d0c5e3a80c9a079a129b6b3e0717e
  - locale: ur
    revision: f1ee2d301e6b
    sha256: 1a95875a2cfbcecdc4d81481a067ea6ce5a493184ea681f85e1314dbe5e98a4b
  - locale: uz
    revision: ea19205d29b9
    sha256: 0ab2c176512d67a4c8cfbb5f91b825613e1aaeba3d7cef92c28e3d885da18902
  - locale: vi
    revision: 8207f57e4ca2
    sha256: 178230f4afa930a7c42525351e13e7bc3a8749a63bd1804494d0c21e652773da
  - locale: xh
    revision: b470aa435c91
    sha256: 76b28da67d466e4681071106d0fc7c254413ac57208dae353b9b31bc7dcd3745
  - locale: zh-CN
    revision: 178364c883f9
    sha256: 62d76f8200291ca97eedb5aaf67f0e05b00ee38c20b48f78144f6c219db17ff1
  - locale: zh-TW
    revision: 31cdae99ca33
    sha256: 4cc84df3a9f8ea6da2d0e1f6c4fc3ad4552c3857c310d7cd83004f95360078b4
