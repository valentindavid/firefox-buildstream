This is an example of packaging of Firefox for both Flatpak and
Snappy. This is not intended to be used but to show how to build
an application to run both on Flatpak and Snappy.

Please see [this blog
post](https://valentindavid.com/posts/2019-03-27-freedesktop-sdk-snap/)
for more information.

### Dependencies

First install BuildStream 1.2 and bst-external:

```
pip3 install --user git+https://gitlab.com/BuildStream/buildstream.git@bst-1
pip3 install --user git+https://gitlab.com/BuildStream/bst-external.git@master
```

There are alternative ways to install it, see the [documentation](https://buildstream.build/install.html).

Then build the `master` branch of Freedesktop SDK and install it:

```
git clone -b 19.08 https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git freedesktop-sdk-snap
cd freedesktop-sdk-snap
make export-snap
snap install --dangerous snap/platform.snap
```

### Building the application

```
git clone https://gitlab.com/valentindavid/firefox-buildstream.git
cd firefox-buildstream
bst build firefox.bst
```

### Testing the application

```
bst shell firefox.bst -- firefox --new-instance
```

### Building and installing the snap

```
bst build snap/image.bst
bst checkout snap/image.bst snap/
snap install --dangerous snap/firefox.snap
/snap/bin/firefox --new-instance
```

### Building and installing the flatpak

```
bst build flatpak/repo.bst
bst checkout flatpak/repo.bst repo/
flatpak remote-add --no-gpg-verify local-firefox repo/
flatpak install local-firefox org.mozilla.Firefox
```
